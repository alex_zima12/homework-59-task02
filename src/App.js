import React, {useEffect,useState} from 'react';
import './App.css';
import Joke from "./Components/Joke/Joke.js"

function App() {

    const [jokes, setJokes] = useState([]);
    const url = 'https://api.chucknorris.io/jokes/random';

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);
            if (response.ok) {
                const jokes = await response.json();
                setJokes(jokes);
            }
        };
        fetchData().catch(e => console.error(e));
    }, []);
    console.log(jokes.value)

    return (
        <div className="Jokes">
                <Joke
                    key={jokes.id}
                    text={jokes.value}
                    img={jokes.icon_url}/>
        </div>

    );
}

export default App;
