import React from 'react';
import "./Joke.css"
const Joke = props => {
    return (
        <div className="Joke" >
            <img src={props.img} alt="" />
            <p>{props.text}</p>
        </div>
    );
};
export default Joke;

